#!/bin/bash
ansible-galaxy install -r requirements.yml --force
ansible-playbook -i inventory main.yml -c local --ask-become-pass