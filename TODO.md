# TODO

- [x] Pull in reqs 
- [x] List out programs to install 
- [x] Directory structure creation 
- [x] ZSH & Dotfiles
- [x] Use collections/roles to optimise install process 
- [x] Add ibus configs
- [x] Add gnome configs
- [ ] Set up program configs where possible
    - [ ] Bitwarden cli for copying sensitive data