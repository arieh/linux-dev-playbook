# Linux Dev Playbook

I got annoyed at setting my laptop up again and again and forgetting all the steps ansible will make it easier

This is specifically aimed for Pop_OS, ymmv if you try this on other distros

## What this does 

- Installs programs I use 
- Configures my system to the way I like it

## How to use 

1. Install ansible

2. Run the `run-playbook-install.sh` script

3. Grab a coffee or something, it'll take a while

## Updating packages?

1. Run the `run-playbook-update.sh` script